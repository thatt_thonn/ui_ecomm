var popca_add_inbox_pedding = false;
var memo_1001_04 = {};
var popca_allItemList = [];
var popca_existMemoCateNm = [];
var popca_cateData=null;


new (Jex.extend({
	onload : function() {
		_this = this;		
		setTimeout(function(){ memo_1001_04.loadInboxData(); }, 200);	
	},
	event : function() {
	
		this.addEvent(".btnNewInbox", "click", function() {
			var allItems_len = $("#allItem").find(".type_edit").length;
			if (allItems_len != 1) {
				$('.btnNewInbox').css("display", "none");
				$(".addNewInbox").css("display", "block");
				$('.addNewInbox').parent().find(".text2").text("");
				$('#popca_memoCateNm').val("");
				$('#popca_memoCateNm').focus();
				popca_add_inbox_pedding = true;
			}
		});

		this.addEvent("#btn_close_inbox", "click", function() {
			var display = $('.addNewInbox').css('display');
			if(display=='block'){
				memo_1001_04.saveMemo($.trim($('#popca_memoCateNm').val()));
			}else{
				top.$("#memo_1001_04_iframe").contents().find("body").empty();
				top.memo.popup.closePopupEvt("memo_1001_04", "#closeEvtBpopup");
			}
		});		
		
	}

}))();

memo_1001_04.toltip_left_out=function($this){
	var offset = $($this).offset();
	$(".popca_tooltiptext_out_left").css("left",parseInt(offset.left)-66);
	$(".popca_tooltiptext_out_left").css("top",parseInt(offset.top)+21);
	$(".popca_tooltiptext_out_left").css("visibility","visible");
}

memo_1001_04.toltip_left_out2=function(){
	$(".popca_tooltiptext_out_left").css("visibility","hidden");
}

memo_1001_04.toltip_right_out=function($this){
	var offset = $($this).offset();
	$(".popca_tooltiptext_out_right").css("left",parseInt(offset.left)+20);
	$(".popca_tooltiptext_out_right").css("top",parseInt(offset.top)-15);
	$(".popca_tooltiptext_out_right").css("visibility","visible");
}

memo_1001_04.toltip_right_out2=function(){
	$(".popca_tooltiptext_out_right").css("visibility","hidden");
}

memo_1001_04.fn_chooseMemoCd = function($this){	
	var memoCateCd = $($this).parent().find('a').attr('data-id');
	$('#MEMO_CATE_CD', window.parent.document).val(memoCateCd);
	var url_string = window.location.href;
	var url = new URL(url_string);
	var page_name = url.searchParams.get("input");
	if(page_name!='"MEMO_CATE"'){
		top.$("#memo_1001_03_iframe").contents().find("input[id=sort_btn]").val("분류함 선택");
		top.$("#memo_1001_03_iframe").contents().find(".sort_btn").val("분류함 선택");
		top.memo.popup.closePopupEvt("memo_1001_04", "#closeEvtBpopup");
	}	
}

memo_1001_04.fn_checkExistMemoCateNm = function(name) {
	popca_existMemoCateNm = [];
	$(popca_allItemList).each(function(i) {
		if (name == this.toLowerCase()) {
			popca_existMemoCateNm.push(true);
			return;
		}
	});
}

memo_1001_04.fn_removeMemoCateNm = function(name) {
	popca_allItemList = jQuery.grep(popca_allItemList, function(value) {
		return value != name;
	});
	console.log("popca_allItemList: ", popca_allItemList);
}

/**
 * create new inbox js
 */
memo_1001_04.fn_keyenter_newInbox=function($this) {
	if (event.key === 'Enter') {
		memo_1001_04.fn_confirm_newInbox($this);
	}
}

memo_1001_04.saveMemo = function(val){
	memo_1001_04.fn_checkExistMemoCateNm(val.toLowerCase());
	if(memo_1001_04.fn_isStringNumber(val)){
		$('#popca_memoCateNm').focus().val("").val(val);
		$('.addNewInbox').find(".text2").text("분류함을 다시 입력해주세요.");
		return;
	}else if(val==""){
		
	}
	else if (popca_existMemoCateNm.length >= 1) {
		var sms = $('.addNewInbox').find(".text2").text("분류함이 이미 존재합니다.");
		$('#popca_memoCateNm').focus().val("").val(val);
		return;
	} else {
		var jexAjax = jex.createAjaxUtil("memo_1001_c02");
		jexAjax.set("MEMO_CATE_NM", val);
		jexAjax.execute(function(dat) {
			if (jex.isError(dat)) {
				alert("조회 중 오류가 발생하였습니다.");
				return;
			}
		});
	}
	top.memo.popup.closePopupEvt("memo_1001_04", "#closeEvtBpopup");
}

memo_1001_04.fn_confirm_newInbox = function($this) {
	var val = $.trim($('#popca_memoCateNm').val());
	memo_1001_04.fn_checkExistMemoCateNm(val.toLowerCase());
	if (val == "") {
		$('#popca_memoCateNm').val("");
		$('#popca_memoCateNm').focus();
		return;
	}else if(memo_1001_04.fn_isStringNumber(val)){
		$('#popca_memoCateNm').focus().val("").val(val);
		$('.addNewInbox').find(".text2").text("분류함을 다시 입력해주세요.");
		return;
	} else if (popca_existMemoCateNm.length >= 1) {
		var sms = $('.addNewInbox').find(".text2").text("분류함이 이미 존재합니다.");
		$('#popca_memoCateNm').focus().val("").val(val);
		return;
	} else {
		popca_add_inbox_pedding = false;
		$('.btnNewInbox').css("display", "block");
		$(".addNewInbox").css("display", "none");
		// create memo_cate
		var jexAjax = jex.createAjaxUtil("memo_1001_c02");
		jexAjax.set("MEMO_CATE_NM", val);
		jexAjax
				.execute(function(dat) {
					if (jex.isError(dat)) {
						alert("조회 중 오류가 발생하였습니다.");
						return;
					} else if (dat.MEMO_CATE_CD != null) {
						popca_allItemList.push(val);
						var textToAdd = '<div class="item">'
								+ '<span class="text">'
								+ val
								+ '</span>'
								+ '<a class="btn_modi" data-val="'
								+ val
								+ '" data-id="'
								+ dat.MEMO_CATE_CD
								+ '" onclick="memo_1001_04.fn_editInBox(this)" href="#none" >수정</a>'
								+ '</div>';
						//$('#allItem').append(textToAdd);
						$(textToAdd).insertAfter($("#allItem .addNewInbox"));
					}
				});
	}
}

memo_1001_04.fn_newInbox_cancel = function() {
	$('#popca_memoCateNm').val("");
	$('.btnNewInbox').css("display", "block");
	$(".addNewInbox").css("display", "none");
	// set new inbox event not happen
	popca_add_inbox_pedding = false;
}

// edit a inbox js
memo_1001_04.fn_editInBox = function($this) {
	var allItems_len = $("#allItem").find(".type_edit").length;
	if (popca_add_inbox_pedding == true) {
	} else if (allItems_len == 1) {
	} else {
		var def_val = $($this).attr("data-val");
		var def_inbox_id = $($this).attr("data-id");
		// get parent key class to replace
		$($this).parent().addClass("type2");
		$($this).parent().addClass("type_edit");
		var content = '<a class="btn_del popca_tooltip" data-name="'+def_val+'" data-id="'
				+ def_inbox_id
				+ '" onclick="memo_1001_04.fn_del_inbox(this)" href="#none" onmouseenter="memo_1001_04.toltip_left_out(this)" onmouseleave="memo_1001_04.toltip_left_out2()">'
				+ '<span class="popca_tooltiptext_out_left">삭제</span>'
				+ '</a>'
				+ '<span class="text"><input type="text" maxlength="100" id="mod_text" onkeydown="memo_1001_04.fn_keyenter_confirmInbox(this)" value="'
				+ def_val + '"></span>'
				+ '<a class="btn_confirm popca_tooltip" id="confirmEdit" data-id="'
				+ def_inbox_id + '" data-val="' + def_val
				+ '" onclick="memo_1001_04.fn_confirm_editInbox()" href="#none" onmouseenter="memo_1001_04.toltip_right_out(this)" onmouseleave="memo_1001_04.toltip_right_out2()">'
				+ '<span class="popca_tooltiptext_out_right">이름바꾸기</span>'
				+ '</a><p class="text2"></p>';
		$($this).parent().empty().append(content);
		$('#mod_text').focus().val("").val(def_val);
	}
}

memo_1001_04.fn_keyenter_confirmInbox = function($this) {
	if (event.key === 'Enter') {
		memo_1001_04.fn_confirm_editInbox();
	}
}

memo_1001_04.fn_confirm_editInbox=function() {
	var inboxId = $('#confirmEdit').attr("data-id");
	var defVal = $('#confirmEdit').attr("data-val");
	var modiText = $.trim($('#mod_text').val());
	memo_1001_04.fn_removeMemoCateNm(defVal);
	memo_1001_04.fn_checkExistMemoCateNm(modiText.toLowerCase());
	if (mod_text == "") {
		$('#mod_text').focus();
		return;
	}else if(memo_1001_04.fn_isStringNumber(modiText)){
		$('#mod_text').focus().val("").val(modiText);
		$('.type_edit').find(".text2").text("분류함을 다시 입력해주세요.");
		return;
	} else if (modiText == defVal) {
		var textToAdd = '<span class="text">'
				+ modiText
				+ '</span>'
				+ '<a class="btn_modi" data-val="'
				+ modiText
				+ '" data-id="'
				+ inboxId
				+ '" onclick="memo_1001_04.fn_editInBox(this)" href="#none" >수정</a>'
		'<p class="text2"></p>';
		$('#confirmEdit').parent().removeClass("type2");
		$('#confirmEdit').parent().removeClass("type_edit");
		$('#confirmEdit').parent().empty().append(textToAdd);
	} else {
		if (popca_existMemoCateNm.length >= 1) {
			$('.type_edit').find(".text2").text("분류함이 이미 존재합니다.");
			$('#mod_text').focus().val("").val(modiText);
			return;
		} else {
			popca_allItemList.push(modiText);
			var jexAjax = jex.createAjaxUtil("memo_1001_u05");
			jexAjax.set("PCSN_DSNC", "N");
			jexAjax.set("MEMO_CATE_CD", inboxId);
			jexAjax.set("MEMO_CATE_NM", modiText);
			jexAjax.execute(function(dat) {
				console.clear();
				console.log("RESP: ", dat);
				if (jex.isError(dat)) {
					alert("조회 중 오류가 발생하였습니다.");
					return;
				}
			});
			var textToAdd = '<span class="text">'
					+ modiText
					+ '</span>'
					+ '<a class="btn_modi" data-val="'
					+ modiText
					+ '" data-id="'
					+ inboxId
					+ '" onclick="memo_1001_04.fn_editInBox(this)" href="#none" >수정</a>'
			'<p class="text2"></p>';
			$('#confirmEdit').parent().removeClass("type2");
			$('#confirmEdit').parent().removeClass("type_edit");
			$('#confirmEdit').parent().empty().append(textToAdd);
		}
	}
}

memo_1001_04.fn_del_inbox=function($this) {
	var memoCateCd = $($this).attr("data-id");
	var memoCateName = $($this).attr("data-name");
	var r = confirm("분류함을 삭제합니다.\n분류함에 있던 메모는 전체메모에서 확인할 수 있습니다. \n\n삭제하시겠습니까? ");
	if (r == true) {
		var jexAjax = jex.createAjaxUtil("memo_1001_d02");
		jexAjax.set("MEMO_CATE_CD", memoCateCd);
		jexAjax.execute(function(dat) {
			if (jex.isError(dat)) {
				alert("조회 중 오류가 발생하였습니다.");
				return;
			}
			memo_1001_04.fn_removeMemoCateNm(memoCateName);
			$($this).parent().remove();
		});
	}
}

memo_1001_04.loadInboxData=function() {
	var pop_wraps='<div id="cateDiv" class="pop_wraps" style="display:none;">'
			+ '<div class="pop_container" style="background: white;">'
			+ '<div class="pop_contents sortModi_area">'
			+ '<h1>분류함 수정</h1>'
			+ '<div class="sort_modi_box mCustomScrollbar">'
			+ '<span class="newSort btnNewInbox" onclick="fn_addNewCategory()"><a href="#none">새 분류함 만들기</a></span>'
			+ '<div class="sort_list" id="allItem">';
	
	var content = '<div class="item type2 addNewInbox">'
			+ '<a class="btn_cancel" onclick="memo_1001_04.fn_newInbox_cancel()" href="#none">취소</a> '
			+ '<span class="text">'
			+ '<input type="text" maxlength="100" id="popca_memoCateNm" value="" onkeydown="memo_1001_04.fn_keyenter_newInbox(this)">'
			+ '</span>'
			+ '<a class="btn_confirm" href="#none" onclick="memo_1001_04.fn_confirm_newInbox(this)">완료</a>'
			+ '<p class="text2"></p>' + '</div>';
			
	var bottom =  '</div>'
				+ '</div>'
				+ '<div class="bottom_area">'
				+ '<a href="#none" id="btn_close_inbox">완료</a>'
				+ '</div>'
				+ '</div>'
				+ '</div>'
				+ '</div>';
				
	var jexAjax = jex.createAjaxUtil("memo_1001_r03");
	jexAjax.execute(function(dat) {
				if (jex.isError(dat)) {
					alert("조회 중 오류가 발생하였습니다.");
					return;
				}
				if (dat.REC != null) {
					$(dat.REC).each(function(i) {
										var title = this.MEMO_CATE_NM;
										if(title.length>40){
											title = title.substring(0,40)+"...";
										}
										content += '<div class="item">'
												+ '<span class="text chooseMemoCd" onclick="memo_1001_04.fn_chooseMemoCd(this)" title="'+title+'">'+ this.MEMO_CATE_NM+ '</span>'
												+ '<a class="btn_modi" data-val="'+ this.MEMO_CATE_NM + '" data-id="'+ this.MEMO_CATE_CD + '" onclick="memo_1001_04.fn_editInBox(this)" href="#none" title="수정">수정</a>'
												+ '</div>';
										popca_allItemList.push(this.MEMO_CATE_NM);
									});
				}				
				
				pop_wraps = pop_wraps+content+bottom;
				$("body").append(pop_wraps);
			});
}



memo_1001_04.fn_isStringNumber(str){
	var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
	if(numberRegex.test(str)) {
	   return true;
	}
	return false;
}

function fn_popUpCate(){
	$("#cateDiv").show();
	 $('#cateDiv').bPopup({
	     follow: [false, false],
	     position: [window.screen.width/2,90]
	 });
}